using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkullProjectille : MonoBehaviour
{
    public float Speed;
    private GameObject Player;
    private Rigidbody2D rb;
    private float Timer;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        Player = GameObject.FindGameObjectWithTag("Player");
        Vector2 Direction = Player.transform.position - this.transform.position;
        rb.velocity = new Vector2(Direction.x, Direction.y).normalized * Speed;
        transform.up = Direction;
        
    }

    void Update(){
        Timer += Time.deltaTime;
        if(Timer > 3){
            Destroy(gameObject);
        }
    }
}
