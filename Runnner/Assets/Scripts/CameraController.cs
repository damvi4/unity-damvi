using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public Transform target;
    public float height = 0;
    public float depth = -10;

    private void LateUpdate()
    {
        if(target.position.y > 1){
            transform.position = new Vector3(
            target.position.x,
            target.position.y - 1,
            depth
            );
        }else{
            transform.position = new Vector3(
            target.position.x,
            height,
            depth
            );
        }
        
    }
    
}
