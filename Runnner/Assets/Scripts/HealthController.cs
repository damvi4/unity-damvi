using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class HealthController : MonoBehaviour
{
    public GameObject Player;
    public float Health;

    void Start()
    {
        ReloadHP();
    }

    public void ReloadHP()
    {
        Health = Player.GetComponent<PlayerController>().HP;
        this.GetComponent<TextMeshProUGUI>().text = " " + Health;
    }
}
