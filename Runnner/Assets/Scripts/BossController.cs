using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossController : MonoBehaviour
{
    private int HP = 10;
    public GameObject Point;
    private GameObject Manager;
    public GameObject Summoned;
    public GameObject Objective1;
    public GameObject Objective2;
    public GameObject Bullet;
    public Transform Position;
    void Start()
    {
        Manager = GameObject.Find("GameManager");
        HP += 10 * Manager.GetComponent<GameManager>().Dificulty;
        StartCoroutine(Spawn());
        StartCoroutine(Shoot());
    }

    private void OnTriggerEnter2D(Collider2D collision){
        if (collision.CompareTag("Blade")){
            this.HP--;

            if(HP == 0)
            {
                Instantiate(Point, transform.position, transform.rotation);
                Destroy(this.gameObject);
            }
        }
    }

    IEnumerator Spawn(){
        while(true){
            yield return new WaitForSeconds(2);
            Instantiate(Summoned, Objective1.transform.position, transform.rotation);
            Instantiate(Summoned, Objective2.transform.position, transform.rotation);
            yield return new WaitForSeconds(2);
            Instantiate(Summoned, Objective1.transform.position, transform.rotation);
            Instantiate(Summoned, Objective2.transform.position, transform.rotation);
            yield return new WaitForSeconds(2);
            Instantiate(Summoned, Objective1.transform.position, transform.rotation);
            Instantiate(Summoned, Objective2.transform.position, transform.rotation);
            yield return new WaitForSeconds(2);
        }
    }

    IEnumerator Shoot(){
        while (true){
            yield return new WaitForSeconds(2);
            Instantiate(Bullet, Position.position, transform.rotation);
            yield return new WaitForSeconds(0.5f);
            Instantiate(Bullet, Position.position, transform.rotation);
            yield return new WaitForSeconds(0.5f);
            Instantiate(Bullet, Position.position, transform.rotation);
            yield return new WaitForSeconds(0.5f);
            Instantiate(Bullet, Position.position, transform.rotation);
            yield return new WaitForSeconds(0.5f);
            Instantiate(Bullet, Position.position, transform.rotation);
            yield return new WaitForSeconds(0.5f);
            Instantiate(Bullet, Position.position, transform.rotation);
            yield return new WaitForSeconds(0.5f);
            Instantiate(Bullet, Position.position, transform.rotation);
            yield return new WaitForSeconds(5);
        }
    }
}