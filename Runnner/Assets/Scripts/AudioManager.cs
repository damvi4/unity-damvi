using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class AudioManager : MonoBehaviour
{
    public AudioClip[] Clips;
    private AudioSource AudioSource;
    void Start()
    {
        AudioSource = this.GetComponent<AudioSource>();
    }

    void Update()
    {
        if(!AudioSource.isPlaying){
            AudioSource.clip= Clips[Random.Range(0, Clips.Length)];
            AudioSource.Play();
        }
    }
}