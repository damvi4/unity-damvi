using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PointController : MonoBehaviour
{

    public float Points = 0;

    void Start()
    {
        GetPoints(0f);
    }

    public void GetPoints(float NewPoints)
    {
        Points += NewPoints;
        this.GetComponent<TextMeshProUGUI>().text = " " + Points;
    }
}
