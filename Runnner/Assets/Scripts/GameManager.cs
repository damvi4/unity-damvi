using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class GameManager : MonoBehaviour
{
    public GameObject StartingRoomEnd;
    private float RoomStartX;
    private float RoomStartY;
    public int GeneratedRooms;
    public List<GameObject> RoomList;
    public int Dificulty = 0;

    void Start()
    {
        RoomStartX = StartingRoomEnd.transform.position.x;
        RoomStartY = StartingRoomEnd.transform.position.y;
        GenerateRooms(GeneratedRooms);
        
    }

    public void GenerateRooms(int numRooms)
    {
        numRooms += Dificulty;
        Dificulty++;
        for(int i = 0; i < numRooms;i++)
        {
            int ChosenRoom = Random.Range(2, RoomList.Count);
            GameObject Room = Instantiate(RoomList[ChosenRoom], new Vector2(RoomStartX, RoomStartY), transform.rotation);
            Transform RoomEnd = Room.transform.Find("RoomEnd");
            RoomStartX = (RoomEnd.transform.position.x);
            RoomStartY = (RoomEnd.transform.position.y);
        }
        GameObject RestRoom = Instantiate(RoomList[1], new Vector2(RoomStartX, RoomStartY), transform.rotation);
        Transform RestRoomEnd = RestRoom.transform.Find("RoomEnd");
        RoomStartX = (RestRoomEnd.transform.position.x);
        RoomStartY = (RestRoomEnd.transform.position.y);
        RestRoom = Instantiate(RoomList[0], new Vector2(RoomStartX, RoomStartY), transform.rotation);
        RestRoom.transform.Find("NewArea").tag="NewArea";
        RestRoomEnd = RestRoom.transform.Find("RoomEnd");
        RoomStartX = (RestRoomEnd.transform.position.x);
        RoomStartY = (RestRoomEnd.transform.position.y);
    }

    public void DeleteRooms(){
        GameObject[] Rooms;
        Rooms = GameObject.FindGameObjectsWithTag("Room");
        for(int i = 0; i < Rooms.Length;i++)
        {
            Destroy(Rooms[i]);
            
        }
        GameObject[] Points;
        Points = GameObject.FindGameObjectsWithTag("5Point");
        for(int i = 0; i < Points.Length;i++)
        {
            Destroy(Points[i]);
            
        }
        Destroy(GameObject.FindGameObjectsWithTag("RestRoom")[0]);
        Destroy(GameObject.FindGameObjectsWithTag("NewArea")[0]);
    }
}