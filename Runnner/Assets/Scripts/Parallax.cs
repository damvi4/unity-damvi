using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parallax : MonoBehaviour
{
    public Vector2 speed;
    private Vector2 offset;
    private Material material;
    public Rigidbody2D player;

    void Start()
    {
        material = GetComponent<SpriteRenderer>().material;
    }

    void Update()
    {
        offset = (player.velocity.x * 0.1f) * speed;
        material.mainTextureOffset += offset;
    }
}
