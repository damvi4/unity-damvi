using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BladeControll : MonoBehaviour
{
    public Transform target;
    public Transform pivot;
    private bool Attacking = false;
    private Animator anim;

    void Start(){
        anim = GetComponent<Animator>();
    }

    void Update()
    {
        pivot.position = new Vector3(
            target.position.x,
            target.position.y,
            target.position.z
            );
        
        if (Input.GetMouseButtonDown(0) && !Attacking)
        {
            StartCoroutine(Attack());
        }
        if(target.transform.localScale.x == -1){
            Vector3 Scale = pivot.transform.localScale;
            Scale.x = -1;
            pivot.transform.localScale = Scale;
        }else{
            Vector3 Scale = pivot.transform.localScale;
            Scale.x = 1;
            pivot.transform.localScale = Scale;
        }
    }

    IEnumerator Attack(){
        Attacking = true;
        anim.SetBool("Swing", true);
        yield return new WaitForSeconds(0.5f);
        anim.SetBool("Swing", false);
        Attacking = false;
    }
}
