using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Start : MonoBehaviour
{
    public void StartGame(){
        SceneManager.LoadScene("Videogame");
    }

    public void GoToTitle(){
        SceneManager.LoadScene("Title");
        Time.timeScale = 1;
    }
}
