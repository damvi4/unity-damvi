using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeconEnemy : MonoBehaviour
{
    private int HP = 3;
    public float Speed;
    private GameObject Player;
    public GameObject Point;
    private Rigidbody2D rb;
    private float Timer;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        Player = GameObject.FindGameObjectWithTag("Player");
        Vector2 Direction = Player.transform.position - this.transform.position;
        rb.velocity = new Vector2(Direction.x, Direction.y).normalized * Speed;
        transform.up = Direction;
    }

    void Update(){
        Timer += Time.deltaTime;
        if(Timer > 2){
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision){
         if(collision.CompareTag("Blade")){
            this.HP--;
            if(HP == 0)
            {
                Instantiate(Point, transform.position, transform.rotation);
                Destroy(this.gameObject);
            }
         }
    }
}
