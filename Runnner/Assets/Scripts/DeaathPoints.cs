using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DeaathPoints : MonoBehaviour
{
    public GameObject points; 
    void Start()
    {
        this.GetComponent<TextMeshProUGUI>().text = "Score: " + points.GetComponent<PointController>().Points;
    }
}
