using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkullShooting : MonoBehaviour
{
    public GameObject Bullet;
    public Transform Position;
    private GameObject Player;
    private float Timer;
    public float AttackRange;
    private Animator anim;
    public GameObject Point;

    public int HP = 2;

    void Start()
    {
        Player = GameObject.FindGameObjectWithTag("Player");
        anim = GetComponent<Animator>();
    }

    void Update()
    {
        
        float Distance =  Vector2.Distance(transform.position, Player.transform.position);

        if (Distance < AttackRange){
            Timer += Time.deltaTime;
        }

        if(Timer > 2){
            Timer = 0;
            StartCoroutine(Shoot());
        }
    }

    private void OnTriggerEnter2D(Collider2D collision){
         if(collision.CompareTag("Blade")){
            this.HP--;
            if(HP == 0)
            {
                Instantiate(Point, transform.position, transform.rotation);
                Destroy(this.gameObject);
            }
         }
    }

    IEnumerator Shoot(){
        anim.SetBool("Shooting", false);
        yield return new WaitForSeconds(0.1f);
        Instantiate(Bullet, Position.position, transform.rotation);
        yield return new WaitForSeconds(0.2f);
        anim.SetBool("Shooting", true);
    }
}
