using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    Rigidbody2D rb;
    public int HP = 5;
    public float Speed;
    public float DashStrength;
    private bool DashCD = false;
    private float Jump = 0.0f;
    public float MaxJump;
    public float JumpSpeed;
    public int MaxJumps = 1;
    private int Jumps;
    private bool Damagable = true;
    public float InmunityTime = 0.5f;
    public GameObject Points;
    public GameObject Health;
    public GameObject DeathScreen;
    public GameObject GameManager;
    private bool NewAreaCD = false;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        Jumps = MaxJumps;
    }

    
    void Update()
    {
        //Movement Left Right
        if (Input.GetKey(KeyCode.D)){
            rb.velocity = new Vector2(Speed, rb.velocity.y);
            GetComponent<SpriteRenderer>().flipX = false;
            Vector3 Scale = transform.localScale;
            Scale.x = 1;
            transform.localScale = Scale;
        }
        else if(Input.GetKey(KeyCode.A)){
            rb.velocity = new Vector2(-Speed, rb.velocity.y);
            Vector3 Scale = transform.localScale;
            Scale.x = -1;
            transform.localScale = Scale;
        }else{
            rb.velocity = new Vector2(0, rb.velocity.y);
        }

        //Dash
        if(Input.GetKeyDown(KeyCode.W) && !DashCD){
            StartCoroutine(Dash());
        }

        //Jump
        if(Input.GetKeyDown(KeyCode.Space)){
            Jumps--;
        }
        if (Input.GetKey(KeyCode.Space) && Jump < MaxJump && Jumps >= 0) {
            if(Jump < 200){
                rb.AddForce(new Vector2 (rb.velocity.x, JumpSpeed*5));
                Jump += JumpSpeed*5;
            }else{
                rb.AddForce(new Vector2 (rb.velocity.x, JumpSpeed));
                Jump += JumpSpeed;
            }
        }
        else if(Input.GetKeyUp(KeyCode.Space) && Jump > 0){
            Jump = 0f;
            rb.AddForce(new Vector2 (rb.velocity.x, -rb.velocity.y*10));
        }

        //Death On Fall
        if(transform.position.y < -10){
            DeathScreen.SetActive(true);
                Time.timeScale = 0;
                Points.SetActive(false);
                Health.SetActive(false);
        }
    }

    void OnCollisionEnter2D(Collision2D col)
    {
       if(col.gameObject.CompareTag("Ground"))
        {
            Jumps = MaxJumps;
       }
       
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Ground"))
        {
            Jumps = MaxJumps;
        }
        if (collision.CompareTag("1Point"))
        {
            Destroy(collision.gameObject);
            Points.GetComponent<PointController>().GetPoints(1f);
        }else if(collision.CompareTag("5Point"))
        {
            Destroy(collision.gameObject);
            Points.GetComponent<PointController>().GetPoints(5f);
        }else if(collision.CompareTag("10Point"))
        {
            Destroy(collision.gameObject);
            Points.GetComponent<PointController>().GetPoints(10f);
        }else if(collision.CompareTag("50Point"))
        {
            Destroy(collision.gameObject);
            Points.GetComponent<PointController>().GetPoints(50f);
        }
        if(collision.gameObject.CompareTag("NewArea") && !NewAreaCD)
        {
            this.HP += 2;
            Health.GetComponent<HealthController>().ReloadHP();
            StartCoroutine(AreaCD());
            GameManager.GetComponent<GameManager>().DeleteRooms();
            GameManager.GetComponent<GameManager>().GenerateRooms(GameManager.GetComponent<GameManager>().GeneratedRooms);
        }
    }
    IEnumerator AreaCD(){
        NewAreaCD = true;
        yield return new WaitForSeconds(1);
        NewAreaCD = false;
    }
    
    private void OnTriggerStay2D(Collider2D collision){
        if((collision.CompareTag("Enemy") || collision.CompareTag("Damaging")) && Damagable){
            HP--;
            if(HP <= 0){
                DeathScreen.SetActive(true);
                Time.timeScale = 0;
                Points.SetActive(false);
                Health.SetActive(false);
            }
            StartCoroutine(Inmunity(InmunityTime));
            Health.GetComponent<HealthController>().ReloadHP();
        }
    }
   IEnumerator Inmunity(float num){
        Damagable = false;
         yield return new WaitForSeconds(num);
        Damagable = true;
    }

    IEnumerator Dash(){
        DashCD = true;
        StartCoroutine(Inmunity(1f));
        this.Speed = Speed*2;
        yield return new WaitForSeconds(0.2f);
        this.Speed = Speed/2;
        DashCD = false;
    }
}